﻿using System;
using System.Collections.Generic;
using MeuAcerto.Selecao.KataGildedRose.Atualizadores;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        ItemClassificador classificador;
        Dictionary<TipoItem, Atualizador> atualizadores;


        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
            this.classificador = new ItemClassificador();
            this.atualizadores = new Dictionary<TipoItem, Atualizador>();

            this.RegistrarAtualizador(new ItemComumAtualizador());
            this.RegistrarAtualizador(new ItemEnvelhecidoAtualizador());
            this.RegistrarAtualizador(new ItemLendarioAtualizador());
            this.RegistrarAtualizador(new IngressosAtualizador());
            this.RegistrarAtualizador(new ItemConjuradoAtualizador());
        }

        public void AtualizarQualidade()
        {
            foreach (var item in this.Itens)
            {
                TipoItem tipoItem = this.classificador.ObterTipo(item);

                if (this.atualizadores.TryGetValue(tipoItem, out Atualizador atualizador))
                {
                    atualizador.Atualizar(item);
                }
                else
                {
                    throw new NotSupportedException($"Nenhum atualizador registrado para o tipo de item {tipoItem}");
                }
            }
        }

        private void RegistrarAtualizador(Atualizador atualizador)
        {
            this.atualizadores[atualizador.Tipo] = atualizador;
        }
    }
}
