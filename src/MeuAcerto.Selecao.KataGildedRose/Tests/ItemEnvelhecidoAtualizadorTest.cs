﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Atualizadores;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemEnvelhecidoAtualizadorTest
    {
        [Fact]
        public void ItemEnvelhecido_QuandoAcimaDoPrazoDeValidade_DeveIncrementarQualidadeEm1()
        {
            // Arrange
            int qualidadeInicial = 4;
            ItemEnvelhecidoAtualizador atualizador = new ItemEnvelhecidoAtualizador();
            Item item = new Item() { Nome = "Item Envelhecido", PrazoParaVenda = 2, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial + 1);
        }

        [Fact]
        public void ItemEnvelhecido_QuandoAbaixoDoPrazoDeValidade_DeveIncrementarQualidadeEm2()
        {
            // Arrange
            int qualidadeInicial = 4;
            ItemEnvelhecidoAtualizador atualizador = new ItemEnvelhecidoAtualizador();
            Item item = new Item() { Nome = "Item Envelhecido", PrazoParaVenda = -1, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial + 2);
        }

        [Fact]
        public void ItemEnvelhecido_QuandoQualidadeIgualA50_NaoDeveIncrementarQualidade()
        {
            // Arrange
            int qualidadeInicial = 50;
            ItemEnvelhecidoAtualizador atualizador = new ItemEnvelhecidoAtualizador();
            Item item = new Item() { Nome = "Item Envelhecido", PrazoParaVenda = -1, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial);
        }
    }
}
