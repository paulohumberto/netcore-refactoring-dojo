﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Atualizadores;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemLendarioAtualizadorTest
    {
        [Fact]
        public void ItemLendario_QuandoAtualizado_DeveManterPrazoParaVendaEQualidade()
        {
            // Arrange
            int prazoParaVendaEsperado = 0;
            int qualidadeEsperada = 80;
            ItemLendarioAtualizador atualizador = new ItemLendarioAtualizador();
            Item item = new Item() { Nome = "Dente Do Tarrasque", PrazoParaVenda = prazoParaVendaEsperado, Qualidade = qualidadeEsperada };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.PrazoParaVenda, prazoParaVendaEsperado);
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }
    }
}
