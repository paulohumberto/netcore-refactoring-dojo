﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Atualizadores;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemConjuradoAtualizadorTest
    {
        [Fact]
        public void ItemConjurado_QuandoAcimaDoPrazoDeValidade_DeveDecrementarQualidadeEm2()
        {
            // Arrange
            int qualidadeInicial = 4;
            ItemConjuradoAtualizador atualizador = new ItemConjuradoAtualizador();
            Item item = new Item() { Nome = "Item Conjurado", PrazoParaVenda = 2, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial - 2);
        }

        [Fact]
        public void ItemConjurado_QuandoAbaixoDoPrazoDeValidade_DeveDecrementarQualidadeEm4()
        {
            // Arrange
            int qualidadeInicial = 6;
            ItemConjuradoAtualizador atualizador = new ItemConjuradoAtualizador();
            Item item = new Item() { Nome = "Item Conjurado", PrazoParaVenda = -1, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial - 4);
        }

        [Fact]
        public void ItemConjurado_QuandoQualidadeForMenorQueODecremento_DeveManterAQualidadeEm0()
        {
            // Arrange
            int qualidadeEsperada = 0;
            ItemConjuradoAtualizador atualizador = new ItemConjuradoAtualizador();
            Item itemAcimaPrazo = new Item() { Nome = "Item Conjurado", PrazoParaVenda = 1, Qualidade = 1 };
            Item itemAbaixoPrazo = new Item() { Nome = "Item Conjurado", PrazoParaVenda = -1, Qualidade = 3 };

            // Act
            atualizador.Atualizar(itemAcimaPrazo);
            atualizador.Atualizar(itemAbaixoPrazo);

            // Assert
            Assert.Equal(itemAcimaPrazo.Qualidade, qualidadeEsperada);
            Assert.Equal(itemAbaixoPrazo.Qualidade, qualidadeEsperada);
        }
    }
}
