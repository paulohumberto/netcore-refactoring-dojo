﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Atualizadores;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class IngressosAtualizadorTest
    {
        [Theory]
        [InlineData(10)]
        [InlineData(11)]
        public void IngressosAualizador_QuandoPrazoMaiorOuIgualA10_IncrementaQualidadeEm1(int prazoParaVenda)
        {
            int qualidadeEsperada = 11;

            // Arrange
            var atualizador = new IngressosAtualizador();
            Item item = new Item() { Nome = "Ingressos", PrazoParaVenda = prazoParaVenda + 1, Qualidade = 10 };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Theory]
        [InlineData(9)]
        [InlineData(5)]
        public void IngressosAualizador_QuandoPrazoMenorQue10_IncrementaQualidadeEm2(int prazoParaVenda)
        {
            int qualidadeEsperada = 12;

            // Arrange
            var atualizador = new IngressosAtualizador();
            Item item = new Item() { Nome = "Ingressos", PrazoParaVenda = prazoParaVenda + 1, Qualidade = 10 };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(1)]
        public void IngressosAualizador_QuandoPrazoMenorQue5_IncrementaQualidadeEm3(int prazoParaVenda)
        {
            int qualidadeEsperada = 13;

            // Arrange
            var atualizador = new IngressosAtualizador();
            Item item = new Item() { Nome = "Ingressos", PrazoParaVenda = prazoParaVenda + 1, Qualidade = 10 };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Fact]
        public void IngressosAualizador_QuandoPrazoMenorQue0_QualidadeIgualA0()
        {
            int qualidadeEsperada = 0;

            // Arrange
            var atualizador = new IngressosAtualizador();
            Item item = new Item() { Nome = "Ingressos", PrazoParaVenda = 0, Qualidade = 10 };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }
    }
}
