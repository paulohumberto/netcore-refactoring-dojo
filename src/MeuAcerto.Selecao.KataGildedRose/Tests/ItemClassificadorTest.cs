﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemClassificadorTest
    {
        [Theory]
        [InlineData("Item comum", TipoItem.Comum)]
        [InlineData("Queijo Brie envelhecido", TipoItem.Envelhecido)]
        [InlineData("QUEIJO BRIE ENVELHECIDO", TipoItem.Envelhecido)]
        [InlineData("queijo brie envelhecido", TipoItem.Envelhecido)]
        [InlineData("Dente do Tarrasque", TipoItem.Lendario)]
        [InlineData("DENTE DO TARRASQUE", TipoItem.Lendario)]
        [InlineData("dente do tarrasque", TipoItem.Lendario)]
        [InlineData("Ingressos para o concerto do Turisas", TipoItem.Ingressos)]
        [InlineData("INGRESSOS PARA O CONCERTO DO TURISAS", TipoItem.Ingressos)]
        [InlineData("ingressos para o concerto do turisas", TipoItem.Ingressos)]
        [InlineData("Bolo de Mana Conjurado", TipoItem.Conjurado)]
        [InlineData("BOLO DE MANA CONJURADO", TipoItem.Conjurado)]
        [InlineData("bolo de mana conjurado", TipoItem.Conjurado)]
        public void ItemClassificador_BaseadoNoNome_DeveRetornarTipoCorreto(string nomeItem, TipoItem tipoEsperado)
        {
            // Arrange
            var classificador = new ItemClassificador();
            Item item = new Item();
            item.Nome = nomeItem;

            // Act
            TipoItem tipo = classificador.ObterTipo(item);

            // Assert
            Assert.Equal(tipo, tipoEsperado);
        }
    }
}
