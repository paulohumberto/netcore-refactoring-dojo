﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Atualizadores;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemComumAtualizadorTest
    {
        [Fact]
        public void ItemComum_QuandoAcimaDoPrazoDeValidade_DeveDecrementarQualidadeEm1()
        {
            // Arrange
            int qualidadeInicial = 4;
            ItemComumAtualizador atualizador = new ItemComumAtualizador();
            Item item = new Item() { Nome = "Item Comum", PrazoParaVenda = 2, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial - 1);
        }

        [Fact]
        public void ItemComum_QuandoAbaixoDoPrazoDeValidade_DeveDecrementarQualidadeEm4()
        {
            // Arrange
            int qualidadeInicial = 6;
            ItemComumAtualizador atualizador = new ItemComumAtualizador();
            Item item = new Item() { Nome = "Item Comum", PrazoParaVenda = -1, Qualidade = qualidadeInicial };

            // Act
            atualizador.Atualizar(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial - 2);
        }

        [Fact]
        public void ItemComum_QuandoQualidadeForMenorQueODecremento_DeveManterAQualidadeEm0()
        {
            // Arrange
            int qualidadeEsperada = 0;
            ItemComumAtualizador atualizador = new ItemComumAtualizador();
            Item itemAcimaPrazo = new Item() { Nome = "Item Comum", PrazoParaVenda = 1, Qualidade = 0 };
            Item itemAbaixoPrazo = new Item() { Nome = "Item Comum", PrazoParaVenda = -1, Qualidade = 1 };

            // Act
            atualizador.Atualizar(itemAcimaPrazo);
            atualizador.Atualizar(itemAbaixoPrazo);

            // Assert
            Assert.Equal(itemAcimaPrazo.Qualidade, qualidadeEsperada);
            Assert.Equal(itemAbaixoPrazo.Qualidade, qualidadeEsperada);
        }
    }
}
