﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public enum TipoItem
    {
        Comum = 0,
        Envelhecido = 1,
        Lendario = 2,
        Ingressos = 3,
        Conjurado = 4,
    }
}
