﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Atualizadores
{
    public class ItemEnvelhecidoAtualizador : ItemComumAtualizador
    {
        public override TipoItem Tipo => TipoItem.Envelhecido;

        protected override int DecrementoBase => -1;
    }
}
