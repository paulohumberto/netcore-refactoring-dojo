﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Atualizadores
{
    public class ItemLendarioAtualizador : Atualizador
    {
        public override TipoItem Tipo => TipoItem.Lendario;

        protected override int QualidadeMinima => 80;

        protected override int QualidadeMaxima => 80;

        protected override void AtualizarPrazoParaVenda(Item item)
        {
        }

        protected override void AtualizarQualidade(Item item)
        {
        }
    }
}
