﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Atualizadores
{
    public class ItemComumAtualizador : Atualizador
    {
        public override TipoItem Tipo => TipoItem.Comum;

        protected virtual int DecrementoBase => 1;

        protected override void AtualizarQualidade(Item item)
        {
            int decremento = this.DecrementoBase;

            if (item.PrazoParaVenda < 0)
            {
                decremento *= 2;
            }

            item.Qualidade -= decremento;
        }
    }
}
