﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Atualizadores
{
    public class IngressosAtualizador : Atualizador
    {
        public override TipoItem Tipo => TipoItem.Ingressos;

        protected override void AtualizarQualidade(Item item)
        {
            if (item.PrazoParaVenda < 0)
            {
                item.Qualidade = 0;
            }
            else if (item.PrazoParaVenda < 5)
            {
                item.Qualidade += 3;
            }
            else if (item.PrazoParaVenda < 10)
            {
                item.Qualidade += 2;
            }
            else
            {
                item.Qualidade++;
            }
        }
    }
}
