﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Atualizadores
{
    public abstract class Atualizador
    {
        public abstract TipoItem Tipo { get; }

        protected virtual int QualidadeMaxima => 50;

        protected virtual int QualidadeMinima => 0;

        public void Atualizar(Item item)
        {
            this.AtualizarPrazoParaVenda(item);
            this.AtualizarQualidade(item);
            this.GarantirQualidadeDentroDoIntervalo(item);
        }

        protected virtual void AtualizarPrazoParaVenda(Item item)
        {
            item.PrazoParaVenda--;
        }

        protected abstract void AtualizarQualidade(Item item);

        protected void GarantirQualidadeDentroDoIntervalo(Item item)
        {
            if (item.Qualidade > this.QualidadeMaxima)
            {
                item.Qualidade = this.QualidadeMaxima;
            }
            
            if (item.Qualidade < this.QualidadeMinima)
            {
                item.Qualidade = this.QualidadeMinima;
            }
        }
    }
}
