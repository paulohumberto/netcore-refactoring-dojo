﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Atualizadores
{
    public class ItemConjuradoAtualizador : ItemComumAtualizador
    {
        public override TipoItem Tipo => TipoItem.Conjurado;

        protected override int DecrementoBase => 2;
    }
}
