﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class ItemClassificador
    {
        private const string QueijoBrieEnvelhecido = "Queijo Brie envelhecido";
        private const string DenteDoTarrasque = "Dente do Tarrasque";
        private const string Ingressos = "Ingressos para o concerto do Turisas";
        private const string Conjurado = "Conjurado";

        public TipoItem ObterTipo(string nomeItem)
        {
            if (nomeItem.Equals(QueijoBrieEnvelhecido, StringComparison.InvariantCultureIgnoreCase))
            {
                return TipoItem.Envelhecido;
            }
            
            if (nomeItem.Equals(DenteDoTarrasque, StringComparison.InvariantCultureIgnoreCase))
            {
                return TipoItem.Lendario;
            }

            if (nomeItem.Equals(Ingressos, StringComparison.InvariantCultureIgnoreCase))
            {
                return TipoItem.Ingressos;
            }

            if (nomeItem.EndsWith(Conjurado, StringComparison.InvariantCultureIgnoreCase))
            {
                return TipoItem.Conjurado;
            }

            return TipoItem.Comum;
        }

        public TipoItem ObterTipo(Item item)
        {
            return this.ObterTipo(item.Nome);
        }
    }
}
